class Translations():
    def __init__(self, lang):
        self.__lang = lang
        self.__data = data

    def get(self, key=None):
        if key and self.__lang:
            return self.__data.get(self.__lang).get(key) or key
        else:
            return key


data = {
    'de': {
        'invalid entry point': 'Ungültiger API Endpunkt',
        'op list not found':
        'Aktuelle OPListe [OPListe_{date}.xlsx] wurde nicht gefunden',
        'customer list not found':
        'Mandantenliste [Mandanten.xlsx] wurde nicht gefunden'
    },
    'en': {
        'invalid entry point': 'invalid entry point',
        'op list not found': 'current op list [OPListe_{date}.xlsx] not found',
        'customer list not found': 'customer list [Mandanten.xlsx] not found'
    }
}
