from os import getenv

import pyodbc
import logging


class Database:
    def __init__(self):
        self.source = getenv('MSSQL_SOURCE', 'live')

    def getCursor(self):
        try:
            SQL_ATTR_CONNECTION_TIMEOUT = 113
            login_timeout = 1
            connection_timeout = 1
            cnxn = pyodbc.connect(
                'DRIVER=/usr/local/lib/libtdsodbc.so;SERVER={server};'
                'PORT=1433;DATABASE={database};UID={uid};PWD={pwd}'
                .format(
                    uid=getenv('MSSQL_USER'), pwd=getenv('MSSQL_PASS'),
                    server=getenv('MSSQL_SERVER'),
                    database=self.getInfo().get('database')
                ),
                timeout=login_timeout,
                attrs_before={
                    SQL_ATTR_CONNECTION_TIMEOUT: connection_timeout
                }
            )

            return cnxn.cursor()

        except Exception as error:
            logging.error(error)
            pass

    def getTables(self, table=None):
        if table:
            return self.getInfo().get('tables').get(table)

        return self.getInfo().get('tables')

    def getInfo(self):
        if self.source == 'live':
            return {
                'database': 'ELBE-FG_80',
                'tables': self.__getLiveDatabase()
            }
        else:
            return {
                'database': 'ELBE-FG_80_TEST',
                'tables': self.__getTestDatabase()
            }

    def __getTestDatabase(self):
        return {
            'easy':  'EASY SECURITY TEST',
            'factoring': 'Elbe Factoring GmbH - TEST',
            'finance': 'Elbe Finance GmbH - TEST',
            'finanzgruppe': 'Elbe Finanzgruppe GmbH - TEST',
            'finetrading': 'Elbe Finetrading GmbH - TEST',
            'finetrading1': 'Elbe Finetrading - KOPIE1',
            'finetrading2': 'Elbe Finetrading - KOPIE2',
            'finetrading3': 'Elbe Finetrading - KOPIE3',
            'collection': 'Elbe Inkasso GmbH - TEST',
            'konsolidierung': 'Elbe Konsolidierung - TEST',
            'sachwerte': 'Elbe Sachwerte GmbH - TEST',
        }

    def __getLiveDatabase(self):
        return {
            'easy': 'EASY SECURITY ECHT',
            'factoring': 'Elbe Factoring GmbH',
            'finance': 'Elbe Finance GmbH LIVE',
            'finanzgruppe': 'Elbe Finanzgruppe AG',
            'finetrading': 'Elbe Finetrading GmbH',
            'collection': 'Elbe Inkasso GmbH',
            'konsolidierung': 'Elbe Konsolidierung',
            'sachwerte': 'Elbe Sachwerte GmbH',
        }
