# coding=utf-8
import logging
from os import getenv
from flask import Flask, current_app, jsonify, render_template, request
from services import Translations


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['LANG'] = 'de'
app.config['DEBUG'] = getenv('DEBUG')
app.config['DB'] = getenv('MSSQL_SOURCE', 'live')

host = getenv('HOST')
port = int(getenv('PORT', '5000'))

i18n = Translations(app.config['LANG'])

if host:
    app.config['SERVER_NAME'] = '{host}:{port}'.format(host=host, port=port)

with app.app_context():
    from routes import contacts, oplist


@app.route('/')
def index():
    return error404()


@app.errorhandler(404)
@app.errorhandler(405)
def error(error):
    return error404()


def error404():
    res = jsonify({
        'status': 'error',
        'message': i18n.get('invalid entry point')
    })
    res.status_code = 404

    return res


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, threaded=True)
