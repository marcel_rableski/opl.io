FROM python:3

# prepare working dir
ADD . /app
WORKDIR /app

# install system requirements
RUN apt-get --assume-yes update
RUN apt-get --assume-yes install gcc unixodbc-dev wget build-essential vim

# find latest version of FreeTDS ftp://ftp.freetds.org/pub/freetds/stable/ and install this version
RUN mkdir -p /app/tmp
WORKDIR /app/tmp
RUN wget ftp://ftp.freetds.org/pub/freetds/stable/freetds-1.00.92.tar.gz
RUN tar -xzf freetds-1.00.92.tar.gz
RUN ls -l
RUN /app/tmp/freetds-1.00.92/configure --prefix=/usr/local --with-tdsver=7.3 && make && make install
WORKDIR /app
RUN rm -r /app/tmp

# install python requirements
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# run
CMD python /app/run.py