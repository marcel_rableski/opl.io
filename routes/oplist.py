# coding=utf-8
import datetime
import logging
from os import getenv, path, system

import pandas as pd
from flask import current_app, jsonify

from services import Translations, Database

i18n = Translations(current_app.config['LANG'])
db = Database()

today = datetime.datetime.today().strftime('%Y%m%d')


@current_app.route('/oplist/import')
def oplistImport():
    # check usage time
    startTime = datetime.datetime.now()

    # set vars
    checkFields = {
        '[Company Name]': 'Mandant', '[Old Contact No_]': 'MandantNr',
        'Address': 'Straße', '[Post Code]': 'PLZ', 'City': 'Ort'
    }
    opExcel = []
    opDB = []
    oplist = []
    queries = []

    try:
        # import excel
        file = path.join(
            '{path}/OPListe_{date}.xlsx'.format(
                path=getenv('OP_PATH'), date=today)
        )
        df = pd.read_excel(file, encoding='utf-8', dtype=str,
                           header=2, keep_default_na='null')
        opExcel = df.query(
            'Konto-Nr != "nan" & Fact.-Mdt == "nan"').to_dict('records')

    except FileNotFoundError:
        message = i18n.get('op list not found').format(date=today)
        logging.error('{0}'.format(message))
        res = jsonify({
            'status': 'cancel',
            'message': message
        })
        res.status_code = 206

        return res

    except Exception as error:
        logging.error(error)
        pass

    try:
        selects = ['{key} as {value}'.format(
            key=key, value=checkFields[key]) for key in checkFields.keys()]

        # connect and fetch data
        cursor = db.getCursor()
        if cursor:
            cursor.execute(
                'SELECT {selects} FROM dbo.[{table}$Contact] '
                'ORDER BY Mandant'.format(
                    selects=','.join(selects),
                    table=db.getTables('factoring')
                )
            )

            columns = [column[0] for column in cursor.description]
            opDB = [dict(zip(columns, row)) for row in cursor.fetchall()]

    except Exception as error:
        logging.error(error)
        pass

    try:
        # filter oplist
        if (len(opExcel) > 0 and len(opDB) > 0):
            for op in opExcel:
                val = {'data': op}
                val['status'] = 'new'

                oplist.append(val)

        json = {
            'status': 'success',
            'op': {
                'allExcel': len(opExcel),
                'allDatabse': len(opDB),
                'statusNew': len(
                    [i for i in oplist if i['status'] == 'new']
                ),
                'statusUpdate': len(
                    [i for i in oplist if i['status'] == 'update']
                ),
                'statusOk': len(
                    [i for i in oplist if i['status'] == 'ok']
                )
            }
        }

        if getenv('DEBUG'):
            json['debug'] = {
                'time': (datetime.datetime.now() - startTime).total_seconds(),
                'database': queries
            }

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        resp = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        resp.status_code = 400

        return resp
