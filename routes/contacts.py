# coding=utf-8
import datetime
import logging
import sys
from os import getenv, path, system

import pandas as pd
from flask import current_app, jsonify

import pyodbc
from services import Translations, Database

i18n = Translations(current_app.config['LANG'])
db = Database()


@current_app.route('/contacts/import')
def contactsImport():
    # check usage time
    startTime = datetime.datetime.now()

    # set vars
    checkFields = {
        '[Company Name]': 'Mandant', '[Old Contact No_]': 'MandantNr',
        'Address': 'Straße', '[Post Code]': 'PLZ', 'City': 'Ort'
    }
    contactExcel = []
    contactDB = []
    contacts = []

    try:
        # import excel
        file = path.join('{path}/Mandanten.xls'.format(path=getenv('OP_PATH')))
        df = pd.read_excel(file, encoding='utf-8', dtype=str, usecols='A:I')
        contactExcel = df.query(
            'Mandant != "nan" & VEnde == "nan"'
        ).to_dict('records')

    except FileNotFoundError:
        res = jsonify({
            'status': 'cancel',
            'message': i18n.get('customer list not found')
        })
        res.status_code = 206

        return res

    except Exception as error:
        logging.error(error)
        pass

    try:
        # connect and fetch data
        selects = ['{key} as {value}'.format(
            key=key, value=checkFields[key]) for key in checkFields.keys()]

        cursor = db.getCursor()
        cursor.execute(
            'SELECT {selects} FROM dbo.[{table}$Contact] '
            'WHERE [Contact Type] = 1'
            'ORDER BY Mandant'.format(
                selects=','.join(selects),
                table=db.getTables('factoring')
            )
        )
        columns = [column[0] for column in cursor.description]
        contactDB = [dict(zip(columns, row)) for row in cursor.fetchall()]

    except Exception as error:
        logging.error(error)
        pass

    try:
        # filter customers
        if (len(contactExcel) > 0):
            for contact in contactExcel:
                val = {'data': contact}
                filteredContact = [
                    i for i in contactDB if i['Mandant'] == contact['Mandant']
                ]
                val.update({'status': 'new'})

                if filteredContact:
                    val.update({'status': 'ok'})
                    fields = {
                        field: [contact[field], filteredContact[0][field]]
                        for field in checkFields.values()
                        if contact[field] != filteredContact[0][field]
                    }

                    if len(fields) > 0:
                        val.update({
                            'status': 'update',
                            'fields': fields
                        })

                    if len(fields) > 2:
                        val.update({
                            'status': 'feedback',
                            'fields': fields
                        })

                contacts.append(val)

            # import new customers in database
            for customer in [i for i in contacts if i['status'] == 'new']:
                columns = checkFields.keys()
                values = [
                    "'{0}'".format(customer.get('data').get(val))
                    for val in checkFields.values()
                    if customer.get('data').get(val)
                ]
                query = []
                for table in db.getTables().values():
                    query.append(
                        'INSERT INTO dbo.[{table}$Contact] ({columns}) '
                        'values ({values})'.format(
                            columns=','.join(columns),
                            values=', '.join(values),
                            table=table
                        ))

                all = ';'.join(query)

                # commit inserts
                # row = cursor.execute(all)
                # cnxn.commit()

            # update outdated customers in database
            for customer in [i for i in contacts if i['status'] == 'update']:
                columns = []
                for field in customer.get('fields').keys():
                    columns.append('{field}=\'{value}\''.format(
                        field=[
                            index
                            for index, value in checkFields.items()
                            if value == field
                        ][0],
                        value=customer.get('data')[field])
                    )

                query = []
                for table in db.getTables().values():
                    query.append(
                        "UPDATE dbo.[{table}$Contact] SET {columns} WHERE "
                        "[Company Name] = '{name}'".format(
                            columns=','.join(columns),
                            name=customer.get('data')['Mandant'],
                            table=table
                        ))

                all = ';'.join(query)

                # commit updates
                # row = cursor.execute(all)
                # cnxn.commit()

        json = {
            'status': 'success',
            'contacts': {
                'allExcel': len(contactExcel),
                'allDatabse': len(contactDB),
                'statusNew': len(
                    [i for i in contacts if i['status'] == 'new']
                ),
                'statusUpdate': len(
                    [i for i in contacts if i['status'] == 'update']
                ),
                'statusFeedback': len(
                    [i for i in contacts if i['status'] == 'feedback']
                ),
                'statusOk': len(
                    [i for i in contacts if i['status'] == 'ok']
                )
            }
        }

        if getenv('DEBUG'):
            json['debug'] = {
                'time': (datetime.datetime.now() - startTime).total_seconds(),
                'contacts': contacts
            }

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res
